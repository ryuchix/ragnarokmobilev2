import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import Notifications from "@/pages/Notifications.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Typography from "@/pages/Typography.vue";
import TableList from "@/pages/TableList.vue";

// users
import Users from "@/pages/Users/Users.vue";
import UpdateProfile from "@/pages/Users/UpdateProfile.vue";
import AddUser from "@/pages/Users/AddUser.vue";
import ChangePassword from "@/pages/Users/ChangePassword.vue";

// jobs
import Jobs from "@/pages/Jobs/Jobs.vue";
import AddJob from "@/pages/Jobs/AddJob.vue";
import EditJob from "@/pages/Jobs/EditJob.vue";

// items
import Items from "@/pages/Items/Items.vue";
import AddItem from "@/pages/Items/AddItem.vue";
import EditItem from "@/pages/Items/EditItem.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard
      },
      {
        path: "users",
        name: "users",
        component: Users
      },
      {
        path: "edit-profile",
        name: "edit profile",
        component: UpdateProfile
      },
      {
        path: "add-user",
        name: "Add user",
        component: AddUser
      },
      {
        path: "change-password",
        name: "Change password",
        component: ChangePassword
      },
      {
        path: "jobs",
        name: "Jobs",
        component: Jobs
      },
      {
        path: "add-job",
        name: "Add job",
        component: AddJob
      },
      {
        path: "edit-job",
        name: "Edit job",
        component: EditJob
      },
      {
        path: "items",
        name: "Items",
        component: Items
      },
      {
        path: "add-item",
        name: "Add item",
        component: AddItem
      },
      {
        path: "edit-item",
        name: "Edit item",
        component: EditItem
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
